# Aprendiendo Docker by angelpc

## About
Comandos muy utiles a la hora de aprender docker:

## Comandos

Directorio root de Docker. Aqui es donde se guarda la info
```bash
$ docker info | grep -i root
$ sudo su
$ cd /var/lib/docker
$ ll
```

### 1. Images

Ver listado de todas las imagenes:
```bash
$ docker images
```

Descargar una imagen:
```bash
$ docker pull my_image_repo
```

Contruir una imagen a partir de un Dockerfile. Se usa `-t` para ponerle un tag/nombre a la imagen que vas a crear:
```bash
$ docker build -t my_image .
```


### 2. Containers

Correr imagen agregando un variable de entorno. Se usar `-d` para hacer run de la imagen en background:
```bash
$ docker run -dti -e "prueba1=12345678" --name my_container_name my_image
```

Para eliminar un contenedores activos. Se usar `-fv` para borrarlo forzosamente:
```bash
$ docker rm -fv my_container_name
```

Para entrar en bash al OS del contenedor. Se usa -ti para terminal interactiva:
```bash
$ docker exec -ti my_containerID bash
```

Para entrar en bash con un usuario especifico `-u` que no sea el default (ejem. root). Ya debe de estar agregado el usuario
```bash
$ docker exec -ti -u angel my_containerID bash
```

Para eliminar todos mis contenedores activos. Se usa `-a` ontenedores activos y `-q` para que solo traiga el container_id:
```bash
$ docker rm -fv $(docker ps -aq)
```

Para ver los logs del contenedor:
```bash
$ docker logs my_container_name
$ docker logs -f my_container_name
```

Para inspecionar el contenedor, Podemos ver el IP del contenedor:
```bash
$ docker inspect my_container_name
```

Copiar algo dentro de un contenedor activo:
```bash
$ docker cp index.html apache:/usr/local/apache2/htdocs
```

Copiar algo que estan dentro del contenedor hacia afuera. El `.` significa al directorio donde estoy:
```bash
$ docker cp apache:/usr/local/apache2/htdocs/index.html .
```

Crear una imagen apartir de un contenedor que esta ejecutandose. NO ES BUENA PRACTICA. Lo que esta dentro de los volumenes no se guarda:
```bash
docker commit my_contenedor_corriendo image_name_resultante
```

Sobrescribir el CMD de un contendor. Lo que se coloca despues de la imagen remplazara el COMMAND. Ejecuta un `docker ps -a` antes y despues:
```bash
$ docker run -dti centos echo hola mundo
$ docker run -d -p 8080:8080 centos pyton -m SimpleHTTPServer 8080
```

Para que el contendor se autodestruya al terminarse de ejecutar:
```bash
$ docker run --rm centos
$ docker ps -a
```

#### Gestion de recursos

Para saber cuanta RAM esta consumiento un contenedor:
```bash
$ docker stats my_container_name
```

Ayuda de docker para memory
```bash
$ docker run --help | grep memo
```

Se usa `-m` para poner un limite de memoria RAM al contenedor
```bash
$ docker run -d -m "500mb" centos
```

Ayuda de docker para memory
```bash
$ docker run --help | grep cpu
```

Ver los CPU tengo
```bash
$ grep "model name" /proc/cpuinfo
```

Ver cuantos CPU tengo. Cuenta el numero de las lineas
```bash
$ grep "model name" /proc/cpuinfo | wc -l
```

Limintar el cantidad de CPU. En este sado se limita a 2 CPU `--cpuset-cpus 0-1` se cuenta desde 0
```bash
$ docker run -d -m "500mb" --cpuset-cpus 0-1 centos
```

## Volumenes

Asignar un volumen `-v docker_host_ruta:conternedor_ruta`. Forma 
```bash
$ docker run -d --name dbprueba1 -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=12345678" -v /opt/mysql/:/var/libmysql mysql:5.7
```

Borrar volumenes Dangling todos
```bash
$ docker volume ls
$ docker volume ls -f dangling=true
$ docker volume ls -f dangling=true -q | xargs docker volume rm
```

#### Volumenes Anonimos

Volument anonimo. Ver la ruta del root. Despeus de ejecutar el contenedor. Vamos a la ruta de la carpeta volumenes dentro de la ruta del root. Se crean con un nombre largo.
```bash
$ docker info | grep -i root
$ $ docker run -d --name dbprueba1 -p 3306:3306 -e "MYSQL_ROOT_PASSWORD=12345678" -v /var/libmysql mysql:5.7
```

Eliminar el contenedor con todo y volumen anonimo `-v`:
```bash
$ docker rm -fv my_contendor
```

#### Volumenes nombrados


## Network

```bash
docker network ls
docker network rm webproxy

```